<?php
namespace MaryJaneInChain\Nazareth;

class Pipeline
{
    const Middleware_Default_Handle_Method = 'handle';
    const Method_Symbol = '@';

    private static $_instance = null;
    protected static $request = null;
    protected static $pipeline = [];

    private function __construct()
    {
        // 单例，防止初始化
    }

    private function __clone()
    {
        // 单例，防止克隆
    }

    public static function getInstance()
    {
        if(is_null(static::$_instance))
            static::$_instance = new static();

        return static::$_instance;
    }

    public static function send(Request $request)
    {
        static::$request = $request;

        return static::getInstance();
    }

    public static function through($pipeline)
    {
        static::$pipeline = is_array($pipeline) ? $pipeline : func_get_args();

        return static::$_instance;
    }

    public static function go(Closure $closure)
    {
        $pipeline = array_reduce(
            static::$pipeline, static::carry, static::prepare($closure)
        );

        return $pipeline(static::$request);
    }

    protected function carry($stack, $pipe)
    {
        return function($stack, $pipe){
            return function($request) use ($stack, $pipe){
                if(is_callable($pipe)){
                    return $pipe($request, $stack);
                }elseif(is_string($pipe)){
                    list($middlewareName, $methodName) = static::spitMiddlewarePattern($pipe);

                    $middlewareClass = new \ReflectionClass($middlewareName);
                    $middleware = $middlewareClass->newInstanceArgs();
                    if($middleware->hasMethod($methodName)){
                        $method = $middleware->getMethod($methodName);
                        return $method->invokeArgs($middleware, [$request, $stack]);
                    }else{
                        throw new Exception('Handler not found');
                    }
                }else{
                    throw new Exception('Uncallable');
                }
            };
        };
    }

    protected function prepare(Closure $closure)
    {
        return function($request) use ($closure){
            return $closure($request);
        };
    }

    protected static function spitMiddlewarePattern($pattern)
    {
        $patternArray = explode(static::Method_Symbol, $pattern);
        if(count($patternArray) == 1)
            $patternArray[] = static::Middleware_Default_Handle_Method;
        if(count($patternArray) != 2)
            throw new Exception('Pattern illegal');

        return $patternArray;
    }
}
