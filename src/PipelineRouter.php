<?php
namespace MaryJaneInChain\Nazareth;

class PipelineRouter extends Router
{
    const Middleware_Default_Handle_Method = 'handle';

    public static function getInstance()
    {
        return parent::getInstance();
    }

    public function action($action, $uri, $callback, $pipeline = [])
    {
        if(!is_array($pipeline))
            $pipeline = array($pipeline);
        array_push($pipeline, $callback);
        $this->routes[$action][$uri] = array_reverse($pipeline);

        return $this;
    }

    public function get($uri, $callback, $pipeline = [])
    {
        return $this->action('get', $uri, $callback, $pipeline);
    }

    public function post($uri, $callback, $pipeline = [])
    {
        return $this->action('post', $uri, $callback, $pipeline);
    }

    public function match(array $actions, $uri, $callback, $pipeline = [])
    {
        foreach($actions as $action){
            $this->action($action, $uri, $callback, $pipeline);
        }

        return $this;
    }

    public function all($uri, $callback, $pipeline = [])
    {
        $this->action('*', $uri, $callback, $pipeline);
    }

    protected function direct($uri, $requestType)
    {
        $type = strtolower($requestType);

        if(array_key_exists($type, $this->routes)){
            foreach($this->routes[$type] as $pattern => $pipeline){
                if($this->isUriMatch($pattern, $uri)){
                    // 如果URI格式符合
                    // TODO: 处理管线
                    $result = array_reduce($pipeline, $this->carry(), $this->request);
                    $result($this->request);
                    var_dump($result);
                    die;
                }
            }
        }
    }

    protected function prepare(Closure $closure)
    {
        return function($request) use ($closure){
            return $closure($request);
        };
    }

    protected function go($request)
    {
        $pipeline = array_reduce(
            static::$pipeline, static::carry, $request
        );

        return $pipeline(static::$request);
    }

    protected function carry()
    {
        return function($stack, $pipe){
            return function($request) use ($stack, $pipe){
                if(is_callable($pipe)){
                    return $pipe($request, $stack);
                }elseif(is_string($pipe)){
                    list($middlewareName, $methodName) = $this->spitMiddlewarePattern($pipe);

                    $middlewareClass = new \ReflectionClass($middlewareName);
                    $middleware = $middlewareClass->newInstanceArgs();

                    if($middlewareClass->hasMethod($methodName)){
                        $method = $middlewareClass->getMethod($methodName);
                        return $method->invokeArgs($middleware, [$request, $stack]);
                    }else{
                        throw new Exception('Handler not found');
                    }
                }else{
                    throw new Exception('Uncallable');
                }
            };
        };
    }

    protected function spitMiddlewarePattern($pattern)
    {
        $patternArray = explode(static::Method_Symbol, $pattern);
        if(count($patternArray) == 1)
            $patternArray[] = static::Middleware_Default_Handle_Method;
        if(count($patternArray) != 2)
            throw new Exception('Pattern illegal');

        return $patternArray;
    }
}
