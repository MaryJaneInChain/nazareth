<?php
namespace MaryJaneInChain\Nazareth;

class Router
{
    const Left_Pattern_Quote = '{';
    const Right_Pattern_Quote = '}';
    const Method_Symbol = '@';

    protected static $_instance = null;
    protected $request = null;
    protected $routes = [
        '*' => [],
    ];

    private function __construct()
    {
        // 单例，防止初始化
    }

    private function __clone()
    {
        // 单例，防止克隆
    }

    public static function getInstance()
    {
        if(is_null(static::$_instance))
            static::$_instance = new static();

        return static::$_instance;
    }

    public function load($files)
    {
        if(!is_array($files))$files = func_get_args();
        $router = static::getInstance();

        $loadClosure = function() use ($files, $router){
            foreach($files as $file){
                require $file;
            }
            return $router;
        };

        return $loadClosure();
    }

    public function action($action, $uri, $callback)
    {
        $action = strtolower($action);
        $this->routes[$action][$uri] = $callback;

        return $this;
    }

    public function get($uri, $callback)
    {
        return $this->action('get', $uri, $callback);
    }

    public function post($uri, $callback)
    {
        return $this->action('post', $uri, $callback);
    }

    public function match(array $actions, $uri, $callback)
    {
        foreach($actions as $action){
            $this->action($action, $uri, $callback);
        }

        return $this;
    }

    public function all($uri, $callback)
    {
        $this->action('*', $uri, $callback);
    }

    public function directByRequest(Request $request)
    {
        $this->request = $request;

        $this->direct(
            $this->request->getPath(),
            $this->request->getMethod()
        );
    }

    protected function direct($uri, $requestType)
    {
        $type = strtolower($requestType);

        if(array_key_exists($type, $this->routes)){
            foreach($this->routes[$type] as $pattern => $callable){
                if($this->isUriMatch($pattern, $uri)){
                    // 如果URI格式符合
                    if(is_string($callable)){
                        list($controllerName, $methodName) = $this->spitControllerPattern($callable);
    
                        // 反射
                        $controllerClass = new \ReflectionClass($controllerName);
                        $controller = $controllerClass->newInstanceArgs();
                        $method = $controllerClass->getMethod($methodName);

                        $params = $this->getUriParams($method, $pattern, $uri);
                        $method->invokeArgs($controller, $params);
                        return;
                    }elseif(is_callable($callable)){
                        $func = new \ReflectionFunction($callable);
                        $params = $this->getUriParams($func, $pattern, $uri);
                        $func->invokeArgs($params);
                        return;
                    }else{
                        throw new Exception('Uncallable');
                    }
                    break;
                }
            }
        }

        // 如果在对应的请求方式中没找到吻合的路由，则在"*"中继续找
        foreach($this->routes['*'] as $pattern =>$callable){
            if($this->isUriMatch($pattern, $uri)){
            // 如果URI格式符合
                if(is_string($callable)){
                    list($controllerName, $methodName) = static::spitControllerPattern($callable);
    
                    // 反射
                    $controllerClass = new \ReflectionClass($controllerName);
                    $controller = $controllerClass->newInstanceArgs();
                    $method = $controllerClass->getMethod($methodName);

                    $params = $this->getUriParams($method, $pattern, $uri);
                    $method->invokeArgs($controller, $params);
                    return;
                }elseif(is_callable($callable)){
                    $func = new \ReflectionFunction($callable);
                    $params = $this->getUriParams($func, $pattern, $uri);
                    $func->invokeArgs($params);
                    return;
                }else{
                    throw new Exception('Uncallable');
                }
                break;
            }
        }

        // TODO: 细化异常类型
        throw new Exception('Page not found');
    }

    protected function getUriParams($func, $pattern, $uri)
    {
        $paramKeys = [];
        foreach($func->getParameters() as $param){
            $paramKeys[] = $param->name;
        }
        $params = array_fill_keys($paramKeys, null);

        $patternArray = explode('/', $pattern);
        $patternArray = array_map(function($key){
            return trim($key, " \t\n\r\0\x0B{}");
        }, $patternArray);
        $uriArray = explode('/', $uri);
        $uriParams = array_combine(array_diff_assoc($patternArray, $uriArray), array_diff_assoc($uriArray, $patternArray));

        foreach($params as $key => $value){
            if(array_key_exists($key, $uriParams))
                $params[$key] = $uriParams[$key];
        }

        return $params;
    }

    protected function isUriMatch($pattern, $uri)
    {
        $regex = '/' . static::Left_Pattern_Quote . '[_0-9a-zA-Z]+' . static::Right_Pattern_Quote . '/';
        $newPattern = preg_replace($regex, '[_0-9a-zA-Z]+', $pattern);
        $newPattern = preg_replace('/\//', '\/', $newPattern);

        $result = preg_match("/$newPattern/", $uri);

        return $result;
    }

    protected function spitControllerPattern($pattern)
    {
        $patternArray = explode(static::Method_Symbol, $pattern);
        if(count($patternArray) != 2) throw new Exception('Pattern illegal');

        // $patternArray[0] is Controller, $patternArray[1] is Method
        return $patternArray;
    }
}
