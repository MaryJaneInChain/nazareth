<?php
namespace MaryJaneInChain\Nazareth;

class Application
{
    protected static $registries = [];

    public static function bind($key, $value)
    {
        static::$registries[$key] = $value;
    }

    public static function get($key)
    {
        if(!array_key_exists($key, static::$registries)){
            // TODO: 细化异常类型
            throw new Exception("No {$key} is bound in the container.");
        }
        return static::$registries[$key];
    }

    public static function ascend()
    {
        // TODO: 改进为管线模式
        static::$registries['router']->directByRequest(static::$registries['request']);
    }
}
