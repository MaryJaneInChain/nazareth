<?php
namespace MaryJaneInChain\Nazareth;

class Request
{
    protected $_path;
    protected $_method;
    protected $_query;
    protected $_request;
    protected $_attributes;
    protected $_cookie;
    protected $_files;
    protected $_server;
    protected $_content;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        return $this->initialize($query, $request, $attributes, $cookies, $files, $server);
    }

    public static function createFromGlobal()
    {
        return new self($_GET, $_POST, [], $_COOKIE, $_FILES, $_SERVER);
    }

    protected function initialize(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        $this->_query       = $query;
        $this->_request     = $request;
        $this->_attributes  = $attributes;
        $this->_cookie      = $cookies;
        $this->_files       = $files;
        $this->_server      = $server;

        $this->_content     = $content;

        if(array_key_exists('REQUEST_URI', $this->_server) && !is_null($this->_server)){
            $this->_path = explode('?', $this->_server['REQUEST_URI'])[0];
        }
        
        if(array_key_exists('REQUEST_METHOD', $this->_server) && !is_null($this->_server)){
            $this->_method = $this->_server['REQUEST_METHOD'];
        }

        return $this;
    }

    public function getParam($key, $default = null)
    {
        $params = $this->_method == 'GET' ? $this->_query : $this->_request;
        return array_key_exists($key, $params) ? $params[$key] : $default;
    }

    public function getParams()
    {
        return $this->_method == 'GET' ? $this->_query : $this->_request;
    }

    public function setParam($key, $value)
    {
        $this->_method == 'GET' ? $this->_query[$key] = $value : $this->_request[$key] = $value; 
    }

    public function getPath()
    {
        return $this->_path;
    }

    public function getMethod()
    {
        return $this->_method;
    }

    public function getQuery()
    {
        return $this->_query;
    }

    public function getRequest()
    {
        return $this->_request;
    }
}
